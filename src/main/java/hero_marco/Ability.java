package hero_marco;

import javax.persistence.*;
import java.util.List;

@Entity
public class Ability {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String potere;
    private String descrizione;

    @ManyToMany(mappedBy="listAbility")
    private List<Hero> listHero;


    public String getPotere() {
        return potere;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public int getId() {
        return id;
    }

    public void setPotere(String potere) {
        this.potere = potere;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }


    public void setId(int id) {
        this.id = id;
    }

}





