package hero_marco;

import org.springframework.data.repository.CrudRepository;


    public interface  AbilityRepository extends CrudRepository<Ability , Integer> {

        Ability findById(int id);

}
