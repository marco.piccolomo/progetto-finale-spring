package hero_marco;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Hero {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NotNull
    @Size(min = 3)
    private String name;
    private String email;


    @ManyToMany
    @JoinTable(
            name="hero_ablity",
            joinColumns=@JoinColumn(name="hero_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="Ability_id", referencedColumnName="id"))
    private List<Ability> listAbility;


    public Hero() {

    }


    public Hero(long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }


    public String getEmail() {
        return email;
    }


    public String getName() {
        return name;

    }

    public long getId() {
        return id;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Ability> getListAbility(){
        return listAbility;
    }

    public void setListAbility(List<Ability> listAbility){this.listAbility= listAbility; }

    }

