package hero_marco;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/hero")
public class MainController {
    @Autowired
    HeroRepository heroRepository;
    @Autowired
    AbilityRepository abilityRepository;

    @CrossOrigin
    @PostMapping("/add")
    @ResponseBody
    public void addNewHero(@RequestBody Hero hero) {
        if (checkEmail(hero.getEmail()))
            heroRepository.save(hero);

    }

    private boolean checkEmail(String email) {
        if (email == null || email.equals("")) {
            return true;
        } else {
            return EmailValidator.getInstance().isValid(email);

        }
    }

    @CrossOrigin
    @GetMapping("/all")
    public @ResponseBody
    Iterable<Hero> getAllUsers() {
        return heroRepository.findAll();
    }

    @CrossOrigin
    @GetMapping("/ability")
    public @ResponseBody
    Iterable<Ability> getAbility() {
        return abilityRepository.findAll();
    }

    @CrossOrigin
    @GetMapping("/byid/{id}")
    public @ResponseBody
    Hero getByid(@PathVariable long id) {
        return heroRepository.findById(id);
    }

    @CrossOrigin
    @GetMapping("/byname/{name}")
    public @ResponseBody
    Hero getByName(@PathVariable String name) {
        return heroRepository.findByName(name);
    }

    @CrossOrigin
    @DeleteMapping("/delete/{id}")
    public void deleteHero(@PathVariable long id) {
        Hero hero = heroRepository.findById(id);
        heroRepository.delete(hero);
    }

    @CrossOrigin
    @PostMapping("/update")
    @ResponseBody
    public ResponseEntity update(@RequestBody Hero heroEmail) {
        try {

            if (checkEmail(heroEmail.getEmail())) {
                Hero hero = heroRepository.findById(heroEmail.getId());
                hero.setEmail(heroEmail.getEmail());
                heroRepository.save(hero);
                return new ResponseEntity(HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            return new ResponseEntity((HttpStatus.BAD_REQUEST));

        }
    }

    @CrossOrigin
    @PostMapping("/addAbility")
    @ResponseBody
    public void addAbility(@RequestBody Ability ability) {
        abilityRepository.save(ability);

    }

    @CrossOrigin
    @PostMapping("/addAbilityHero/{id}")
    @ResponseBody
    public void addAbilityHero(@PathVariable long id, @RequestBody Ability ability) {
        Hero hero = heroRepository.findById(id);
        List<Ability> listAbility = hero.getListAbility();
        listAbility.add(ability);
        hero.setListAbility(listAbility);
        heroRepository.save(hero);
    }

    @CrossOrigin
    @DeleteMapping("/deleteAbility/{id}")
    public void deleteAbility(@PathVariable int id) {
        Ability ablity = abilityRepository.findById(id);
        System.out.println(id);
        abilityRepository.delete(ablity);

    }
}









