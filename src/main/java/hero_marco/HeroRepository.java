package hero_marco;

import org.springframework.data.repository.CrudRepository;

import hero_marco.Hero;

public interface HeroRepository extends CrudRepository <Hero,Integer>{
 Hero findById ( long id) ;
 Hero findByName  (String name) ;

}

